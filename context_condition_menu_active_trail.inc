<?php

class menu_active_trail_condition extends context_condition {
  function condition_values() {
    return menu_get_menus(true);
  }

  function execute() {
    if ($this->condition_used()) {
      $active_trail = menu_get_active_trail();

      foreach ($active_trail as $item) {
        if (!isset($item['menu_name'])) {
          continue;
        }

        foreach ($this->get_contexts($item['menu_name']) as $context) {
          $this->condition_met($context, $item['menu_name']);
        }
      }
    }
  }
}
